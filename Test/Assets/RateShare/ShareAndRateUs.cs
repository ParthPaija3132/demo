﻿using UnityEngine;
using System.Collections;

public class ShareAndRateUs : MonoBehaviour
{
    #region Public_Variables
    [Tooltip("iOS App ID (number), example: 1122334455")]
    public string iOSAppID = "";
    #endregion

    #region Private_Variables
    private const string iOSRatingURI = "itms://itunes.apple.com/us/app/apple-store/{0}?mt=8";
    #endregion

    #region Events
    #endregion

    #region Unity_CallBacks

    #endregion

    #region Private_Methods
    #endregion

    #region Public_Methods
    public void Share()
    {
        Debug.Log("Share");
        string url = string.Empty;
#if UNITY_IOS
        if (!string.IsNullOrEmpty(iOSAppID))
        {
            url = iOSRatingURI.Replace("{0}", iOSAppID);
        }
        else
        {
            Debug.LogWarning("Please set iOSAppID variable");
        }
#endif
        new NativeShare().SetText(url).Share();
    }



    public void RateUs()
    {
        Debug.Log("Rate Us");
        string url = string.Empty;
#if UNITY_IOS
        if (!string.IsNullOrEmpty(iOSAppID))
        {
            url = iOSRatingURI.Replace("{0}", iOSAppID);
        }
        else
        {
            Debug.LogWarning("Please set iOSAppID variable");
        }
#endif
        Application.OpenURL(url);
    }
    #endregion

    #region UI_Calls
    #endregion

    #region Coroutines
    #endregion

    #region Custom_CallBacks
    #endregion
}

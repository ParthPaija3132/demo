﻿using UnityEngine;
using System.Collections;

namespace tag
{
	public class PauseView : BaseView
 {
	#region PUBLIC_VARS
    #endregion

    #region PRIVATE_VARS
    #endregion

    #region UNITY_CALLBACKS
    #endregion

    #region PUBLIC_FUNCTIONS
    #endregion

    #region PRIVATE_FUNCTIONS
    #endregion

    #region CO-ROUTINES
    #endregion

    #region EVENT_HANDLERS
    #endregion

	#region UI_CALLBACKS
		public void OnHome(){
			Time.timeScale = 1;
			HideView ();
			UIManager.Instance.gameplayView.HideView ();
			UIManager.Instance.homeView.ShowView ();
            SoundManager.Instance.PlayUIClick();
		}

		public void OnResume ()
		{
			GameManager.Instance.ResumeGame ();
            SoundManager.Instance.PlayUIClick();
		}

	#endregion
}
}
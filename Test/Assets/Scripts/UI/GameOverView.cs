﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace tag
{
    public class GameOverView : BaseView
    {
        #region PUBLIC_VARS
        public Text Score;
        public Text BestScore;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACK

        void OnEnable()
        {
            SoundManager.Instance.StopOtherBGMusic();
            SoundManager.Instance.PlayGameover();
            Score.text = DataManager.Instance.score.ToString();
            BestScore.text = DataManager.Instance.playerData.highScore.ToString();
        }

        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS

        public void OnHome()
        {
            HideView();
            UIManager.Instance.homeView.ShowView();
            SoundManager.Instance.PlayUIClick();
        }

        public void OnRePlay()
        {
            HideView();
            GameManager.Instance.RestartGame();
            SoundManager.Instance.PlayUIClick();
        }

        public void OnShare()
        {
            SoundManager.Instance.PlayUIClick();
        }

        public void OnRate()
        {
            SoundManager.Instance.PlayUIClick();
        }

        #endregion
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace tag
{
    public class UpgradeView : BaseView
    {
        #region PUBLIC_VARS

        public Text totalPoints;
        public Text speedLevelText;
        public Text powerLevelText;
        public Text speedAmountText;
        public Text powerAmountText;

        public GameObject goSpeedUpgrade;
        public GameObject goSpeedFullUpgrade;

        public GameObject goPowerUpgrade;
        public GameObject goPowerFullUpgrade;

        #endregion

        #region PRIVATE_VARS

        private UpgardeDataSO speedUpgrade;
        private UpgardeDataSO powerUpgrade;

        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS

        public override void ShowView()
        {
            base.ShowView();
            SetView();
        }

        #endregion

        #region PRIVATE_FUNCTIONS

        private void SetView()
        {
            DataManager dataManager = DataManager.Instance;
            speedUpgrade = dataManager.shootingSpeedUpgrade;
            powerUpgrade = dataManager.shootingPowerUpgrade;

            totalPoints.text = dataManager.playerData.totalPoint.ToString();
            speedLevelText.text = "LEVEL" + (dataManager.playerData.shootingSpeedUpgradeIndex + 1).ToString();
            powerLevelText.text = "LEVEL" + (dataManager.playerData.shootingPowerUpgardeIndex + 1).ToString();

            if (dataManager.playerData.shootingSpeedUpgradeIndex + 1 < speedUpgrade.upgradeDatas.Count)
            {
                goSpeedUpgrade.SetActive(true);
                goSpeedFullUpgrade.SetActive(false);
                speedAmountText.text = speedUpgrade.upgradeDatas[dataManager.playerData.shootingSpeedUpgradeIndex + 1].amount.ToString();
            }
            else
            {
                goSpeedUpgrade.SetActive(false);
                goSpeedFullUpgrade.SetActive(true);
            }


            if (dataManager.playerData.shootingPowerUpgardeIndex + 1 < powerUpgrade.upgradeDatas.Count)
            {
                goPowerUpgrade.SetActive(true);
                goPowerFullUpgrade.SetActive(false);
                powerAmountText.text = powerUpgrade.upgradeDatas[dataManager.playerData.shootingPowerUpgardeIndex + 1].amount.ToString();
            }
            else
            {
                goPowerUpgrade.SetActive(false);
                goPowerFullUpgrade.SetActive(true);
            }
        }

        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS

        public void OnClose()
        {
            HideView();
            UIManager.Instance.homeView.ShowView();
        }

        public void OnSpeedUpgrade()
        {
            if (DataManager.Instance.playerData.totalPoint >= speedUpgrade.upgradeDatas[DataManager.Instance.playerData.shootingSpeedUpgradeIndex + 1].amount)
            {
                DataManager.Instance.playerData.shootingSpeedUpgradeIndex++;
                DataManager.Instance.playerData.shootingSpeedUpgradeIndex = Mathf.Clamp(DataManager.Instance.playerData.shootingSpeedUpgradeIndex, 0, DataManager.Instance.shootingSpeedUpgrade.upgradeDatas.Count);
                DataManager.Instance.SavePlayerData();
                SetView();
            }
        }

        public void OnPowerUpgrde()
        {
            if (DataManager.Instance.playerData.totalPoint >= powerUpgrade.upgradeDatas[DataManager.Instance.playerData.shootingPowerUpgardeIndex + 1].amount)
            {
                DataManager.Instance.playerData.shootingPowerUpgardeIndex++;
                DataManager.Instance.playerData.shootingPowerUpgardeIndex = Mathf.Clamp(DataManager.Instance.playerData.shootingPowerUpgardeIndex, 0, DataManager.Instance.shootingPowerUpgrade.upgradeDatas.Count);
                DataManager.Instance.SavePlayerData();
                SetView();
            }
        }

        #endregion
    }
}
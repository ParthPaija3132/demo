﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace tag
{
    public class GameplayView : BaseView
    {
        #region PUBLIC_VARS
		public Text scoreText;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS

		void OnEnable ()
		{
            SoundManager.Instance.PlayGameplaySound();
            OnScoreChange(0);
            EventManager.onScoreChange += OnScoreChange;
		}

		private void OnDisable()
		{
            EventManager.onScoreChange -= OnScoreChange;
		}

		#endregion

		#region PUBLIC_FUNCTIONS

		public void OnScoreChange(int value)
		{
            scoreText.text = DataManager.Instance.score.ToString();
		}

        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS

		public void OnPause ()
		{
			GameManager.Instance.PauseGame ();
            SoundManager.Instance.PlayUIClick();
		}

        #endregion
    }
}
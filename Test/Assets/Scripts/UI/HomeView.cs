﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace tag
{
    public class HomeView : BaseView
    {
        #region PUBLIC_VARS

        public Image soundRefImage;
        public Sprite soundOnSprite;
        public Sprite soundOffSprite;
        public Text shootingPowerText;
        public Text shootingSpeedText;
        public Text highScoreText;

        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS

        public override void ShowView()
        {
            base.ShowView();
            SetView();
            UIManager.Instance.upgradeView.HideView();
        }

        #endregion

        #region PRIVATE_FUNCTIONS

        private void SetView()
        {
            SetScoreText();
            SetSprite(SoundManager.Instance.IsSoundOn);
            SetUpgradeData();
            SoundManager.Instance.PlayHomeBackgroundSound();
        }

        private void SetScoreText()
        {
            highScoreText.text = DataManager.Instance.playerData.highScore.ToString();
        }

        private void SetUpgradeData()
        {
            DataManager dataManager = DataManager.Instance;

            float shootingSpeedUpgradeValue = dataManager.shootingSpeedUpgrade.upgradeDatas[dataManager.playerData.shootingSpeedUpgradeIndex].value;
            float shootingPowerUpgradeValue = dataManager.shootingPowerUpgrade.upgradeDatas[dataManager.playerData.shootingPowerUpgardeIndex].value;

            shootingPowerText.text = shootingPowerUpgradeValue.ToString();
            shootingSpeedText.text = shootingSpeedUpgradeValue.ToString();
        }

        private void SetSprite(bool soundStatus)
        {
            if (soundStatus)
            {
                soundRefImage.sprite = soundOnSprite;
            }
            else
            {
                soundRefImage.sprite = soundOffSprite;
            }
        }

        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS

        public void OnPlay()
        {
            HideView();
            SoundManager.Instance.PlayUIClick();
            GameManager.Instance.StartGame();
        }

        public void OnSound()
        {
            SoundManager.Instance.ToggleSound();
            SetSprite(SoundManager.Instance.IsSoundOn);
            SoundManager.Instance.PlayUIClick();
        }

        public void OnShare()
        {
            SoundManager.Instance.PlayUIClick();
        }

        public void OnRate()
        {
            SoundManager.Instance.PlayUIClick();
        }

        public void OnUpgrade()
        {
            UIManager.Instance.upgradeView.ShowView();
        }

        #endregion
    }
}
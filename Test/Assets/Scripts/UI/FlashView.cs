﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

namespace tag
{
    public class FlashView : BaseView {

        #region PUBLIC_VARS
        public Image splashImage;

        public float animationTime;
        public AnimationCurve animationCurve;
		#endregion

		#region PRIVATE_VARS

		#endregion

		#region UNITY_CALLBACKS

		#endregion

		#region PUBLIC_FUNCTIONS
        public void ShowSplash()
        {
            ShowView();
            StartCoroutine(ShowSplashEffect());
        }
		#endregion

		#region PRIVATE_FUNCTIONS
        private Color GetColorWithAlpha(Color color , float alpha)
        {
            Color colorToReturn = color;
            colorToReturn.a = alpha;
            return colorToReturn;
        }
		#endregion

		#region CO-ROUTINES
        IEnumerator ShowSplashEffect()
        {
            float t = 0;
            float rate = 1 / animationTime;

            while(t <= 1f)
            {
                t += rate * Time.deltaTime;
                splashImage.color = GetColorWithAlpha(splashImage.color,animationCurve.Evaluate(t));
                yield return 0;
            }

            HideView();
        }
		#endregion

		#region EVENT_HANDLERS

		#endregion

		#region UI_CALLBACKS

		#endregion
	}
}
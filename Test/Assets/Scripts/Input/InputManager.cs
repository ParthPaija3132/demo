﻿using UnityEngine;
using System.Collections;
using System;

namespace tag
{
    public class InputManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static InputManager Instance;
        public BaseInput input;
        public BaseInput[] inputs;
        public bool IsInputAllowed{
            get { return isInputAllowed; }
        }
        #endregion

        #region PRIVATE_VARS
        private bool isInputAllowed;
        #endregion

        #region UNITY_CALLBACKS
        void Awake()
        {
            Instance = this;
            SetInput(InputType.PC);
            SetActive(false);
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public void SetActive(bool isActive)
        {
            input.gameObject.SetActive(isActive);
        }

        public void EnableInput()
        {
            isInputAllowed = true;
        }

        public void DisableInput()
        {
            isInputAllowed = false;
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        private void SetInput(InputType inputType)
        {
            for (int i = 0; i < inputs.Length; i++)
            {
                inputs[i].gameObject.SetActive(false);
                if (inputs[i].type == inputType)
                    input = inputs[i];
            }
            SetActive(true);
        }
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
    public enum InputType
    {
        PC,
        MOBILE
    }
}
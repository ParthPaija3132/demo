﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tag
{
    public class ObstacalGenrater : MonoBehaviour
    {
        #region PUBLIC_VARS

        public float timeToGenrateObstacal;
        public List<Obstacal> obstacals;

        #endregion

        #region PRIVATE_VARS

        private Coroutine genaterCO;

        #endregion

        #region UNITY_CALLBACKS

        private void OnEnable()
        {
            EventManager.onGameStart += StartGenrater;
            EventManager.onGameOver += StopGenrater;
        }

        private void OnDisable()
        {
            EventManager.onGameStart -= StartGenrater;
            EventManager.onGameOver -= StopGenrater;
        }

        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS

        private void StartGenrater()
        {
            genaterCO = StartCoroutine(DoGenrateObstacal());
        }

        private void StopGenrater()
        {
            StopCoroutine(genaterCO);
            Utility.DestroyChildren(transform);
        }

        #endregion

        #region CO-ROUTINES

        private IEnumerator DoGenrateObstacal()
        {
            while (true)
            {
                GenrateObstacal();
                yield return new WaitForSeconds(2f);
            }
        }

        private void GenrateObstacal()
        {
            Instantiate(GetObstacal(), transform.position, Quaternion.identity, transform);
        }

        private Obstacal GetObstacal()
        {
            return obstacals[UnityEngine.Random.Range(0, obstacals.Count)];
        }

        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
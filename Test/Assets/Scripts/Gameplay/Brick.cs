﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace tag
{
    public class Brick : MonoBehaviour
    {
        #region PUBLIC_VARS

        public Text damageText;
        public Color[] colors;
        public SpriteRenderer spriteRenderer;
        public ParticleSystem ps;

        #endregion

        #region PRIVATE_VARS

        private int damageAmount;
        private int bullateDamage;

        #endregion

        #region UNITY_CALLBACKS

        private void Start()
        {
            SetBrickDamage();
            SetBrickColor();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag(Constant.BULLATE))
            {
                bullateDamage = collision.GetComponent<Bullate>().bullateDamage;
                SoundManager.Instance.PlayCollect();
                TakeDamage(bullateDamage);
                Destroy(collision.gameObject);
            }
            else if (collision.CompareTag(Constant.PLAYER))
            {
                SoundManager.Instance.PlayExplosion();
                GameManager.Instance.GameOver();
            }
        }

        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS

        internal void SetBrickColor()
        {
            if (damageAmount <= 10)
            {
                spriteRenderer.color = colors[0];
            }
            else if (damageAmount > 10 && damageAmount <= 30)
            {
                spriteRenderer.color = colors[1];
            }
            else if (damageAmount > 30 && damageAmount <= 50)
            {
                spriteRenderer.color = colors[2];
            }
            else
            {
                spriteRenderer.color = colors[3];
            }
        }

        internal void SetBrickDamage()
        {
            float speedValue = DataManager.Instance.shootingSpeedUpgrade.upgradeDatas[DataManager.Instance.playerData.shootingSpeedUpgradeIndex].value;
            float powerValue = DataManager.Instance.shootingPowerUpgrade.upgradeDatas[DataManager.Instance.playerData.shootingPowerUpgardeIndex].value;
            damageAmount = Random.Range(1, 10);
            damageAmount = (int)(damageAmount * speedValue * powerValue);
            damageText.text = damageAmount.ToString();
        }

        private void TakeDamage(int damage)
        {
            damageAmount -= damage;
            DataManager.Instance.IncreaseScore(damage);
            damageText.text = damageAmount.ToString();
            SetBrickColor();
            if (damageAmount <= 0)
            {
                if (ps != null)
                {
                    ps.Play();
                }
                Destroy(gameObject);
            }
        }

        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
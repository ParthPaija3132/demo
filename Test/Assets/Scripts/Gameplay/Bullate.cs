﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tag
{
    public class Bullate : MonoBehaviour
    {
        #region PUBLIC_VARS

        public Rigidbody2D rb;
        public float speed;
        public int bullateDamage = 1;

        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS

        private void Start()
        {
            Destroy(gameObject, 0.8f);
        }

        private void FixedUpdate()
        {
            Move();
        }

        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS

        private void Move()
        {
            rb.velocity = Vector2.up * speed * Time.fixedDeltaTime;
        }

        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
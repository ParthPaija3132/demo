﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tag
{
    public class Obstacal : MonoBehaviour
    {
        #region PUBLIC_VARS

        public float speed;

        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            transform.Translate(Vector2.down * speed * Time.deltaTime);
        }

        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
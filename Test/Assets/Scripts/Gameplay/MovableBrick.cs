﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tag
{
	public class MovableBrick : Brick
	{
		#region PUBLIC_VARS

		public Transform pointA;
		public Transform pointB;
        public float speed;

        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS

        private void Start()
        {
            SetBrickDamage();
            SetBrickColor();
            StartCoroutine(DoMove());
        }

        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES

        IEnumerator DoMove()
        {
            while (true)
            {
                if (transform.position != pointB.position)
                {
                    transform.position = Vector3.MoveTowards(transform.position, pointB.position, Time.fixedDeltaTime * speed);
                    yield return new WaitForFixedUpdate();
                }
                else
                {
                    Transform temp = pointA;
                    pointA = pointB;
                    pointB = temp;
                }
            }

        }

        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
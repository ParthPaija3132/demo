﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;

namespace tag
{
    public class Player : MonoBehaviour
    {
        #region PUBLIC_VARS

        public Camera myCamera;
        public float movementSpeed;
        public Bullate bullatePrefab;
        public Transform firePoint;
        public float shootingSpeed;
        public BoxCollider2D boxCollider;
        public StandaloneInputModule standalone;

        #endregion

        #region PRIVATE_VARS

        private Vector2 mousePosition;
        private float postionX;
        private float maxPostionX;
        private Coroutine bullateGenrateCO;
        [SerializeField] private GameObject currentFocusGameObject;

        #endregion

        #region UNITY_CALLBACKS

        private void OnEnable()
        {
            EventManager.onGameStart += OnGameStart;
            EventManager.onGameStart += StartGenrateBullate;
            EventManager.onGameOver += StopGenrateBullate;
        }

        private void OnDisable()
        {
            EventManager.onGameStart -= OnGameStart;
            EventManager.onGameStart -= StartGenrateBullate;
            EventManager.onGameOver -= StopGenrateBullate;
        }

        private void Start()
        {
            maxPostionX = myCamera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x;
        }

        private void Update()
        {
            FieldInfo pi = standalone.GetType().GetField("m_CurrentFocusedGameObject", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            currentFocusGameObject = (GameObject)pi.GetValue(standalone);
            if (Input.GetMouseButton(0))
            {
                if (currentFocusGameObject == null)
                {
                    MovePlayer();
                }
            }
        }

        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS

        private void OnGameStart()
        {
            shootingSpeed /= DataManager.Instance.shootingSpeedUpgrade.upgradeDatas[DataManager.Instance.playerData.shootingSpeedUpgradeIndex].value;
        }

        private void StartGenrateBullate()
        {
            boxCollider.enabled = true;
            bullateGenrateCO = StartCoroutine(DoFire());
        }

        private void StopGenrateBullate()
        {
            boxCollider.enabled = false;
            shootingSpeed = 0.2f;
            StopCoroutine(bullateGenrateCO);
        }

        private void MovePlayer()
        {
            mousePosition = myCamera.ScreenToWorldPoint(Input.mousePosition);
            postionX = Mathf.Clamp(mousePosition.x, -maxPostionX, maxPostionX);
            transform.position = Vector2.Lerp(transform.position, new Vector2(postionX, transform.position.y), movementSpeed * Time.deltaTime);
        }

        private void GenrateBullate()
        {
            Bullate bullate = Instantiate(bullatePrefab, firePoint.position, Quaternion.identity);
            bullate.bullateDamage = (int)DataManager.Instance.shootingPowerUpgrade.upgradeDatas[DataManager.Instance.playerData.shootingPowerUpgardeIndex].value;
        }

        #endregion

        #region CO-ROUTINES

        private IEnumerator DoFire()
        {
            while (true)
            {
                GenrateBullate();
                yield return new WaitForSeconds(shootingSpeed);
            }
        }

        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
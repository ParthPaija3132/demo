﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "UpgradesData", menuName = "ScriptableObjects/Upgrade/UpgradesData", order = 1)]
public class UpgardeDataSO : ScriptableObject
{
    #region PUBLIC_VARS

    public UpgradeType upgradeType;
    public List<UpgradeData> upgradeDatas;

    #endregion

    #region PRIVATE_VARS
    #endregion

    #region UNITY_CALLBACKS
    #endregion

    #region PUBLIC_FUNCTIONS
    #endregion

    #region PRIVATE_FUNCTIONS
    #endregion

    #region CO-ROUTINES
    #endregion

    #region EVENT_HANDLERS
    #endregion

    #region UI_CALLBACKS
    #endregion
}

[System.Serializable]
public class UpgradeData
{
    public int amount;
    public float value;
}

public enum UpgradeType
{
    SHOOTING_SPEED,
    SHOOTING_POWER
}
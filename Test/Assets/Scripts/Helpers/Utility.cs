﻿using UnityEngine;
using System.Collections;
using System;
using Newtonsoft.Json;

namespace tag
{
    public class Utility
    {
        #region PUBLIC_FUNCTIONS
        public static void DestroyChildren(Transform parent)
        {
            for (int i = parent.childCount - 1; i >= 0; i--)
            {
                GameObject.Destroy(parent.GetChild(i).gameObject);
            }
        }
        public static void DestroyChildrenImmediate(Transform parent)
        {
            for (int i = parent.childCount - 1; i >= 0; i--)
            {
                GameObject.DestroyImmediate(parent.GetChild(i).gameObject);
            }
        }
        public static string SerializeObject(object objToSerialize)
        {
            return JsonConvert.SerializeObject(objToSerialize);
        }

        public static T DeserializeObject<T>(string serializedString)
        {
            return JsonConvert.DeserializeObject<T>(serializedString);
        }

        public static LayerMask LayerNumbersToMask(params int[] layerNumbers)
        {
            LayerMask ret = (LayerMask)0;
            for (int i = 0; i < layerNumbers.Length; i++)
            {
                ret |= (1 << layerNumbers[i]);
            }
            return ret;
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

    }
}
﻿using UnityEngine;
using System.Collections;

namespace tag
{
    public class EventManager
    {
        #region PUBLIC_VARS
		public delegate void OnGameStart ();

		public static event OnGameStart onGameStart;

		public static void RaiseOnGameStart ()
		{
			if (onGameStart != null)
				onGameStart ();
		}


		public delegate void OnGameOver ();

		public static event OnGameOver onGameOver;

		public static void RaiseOnGameOver ()
		{
			if (onGameOver != null)
				onGameOver ();
		}


		public delegate void OnScoreChange (int value);

		public static event OnScoreChange onScoreChange;

		public static void RaiseOnScoreChange (int value)
		{
			if (onScoreChange != null)
				onScoreChange (value);
		}


        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
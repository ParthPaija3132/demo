﻿using UnityEngine;
using System.Collections;

namespace tag
{
    public class SoundManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static SoundManager Instance;

        [Header("Sound Refrences")]
        public AudioSource backgroundMusic;
        public AudioSource gameplayMusic;
        public AudioSource uiClick;
        public AudioSource collisionExplosionMusic;
        public AudioSource shootingSound;
        public AudioSource gameOverSound;

        public bool IsSoundOn
        {
            get 
            {
                return isSoundOn;
            }
        }
        #endregion

        #region PRIVATE_VARS
        private bool isSoundOn;
        private AudioSource bgMusic;
        #endregion

        #region UNITY_CALLBACKS
        void Awake()
        {
            Instance = this;
            SetSound();
        }

        //private void OnEnable()
        //{
        //    PlayHomeBackgroundSound();
        //}
        #endregion

        #region PUBLIC_FUNCTIONS
        public void ToggleSound()
        {
            if(isSoundOn)
            {
                DisableSound();
            }else{
                EnableSound();
            }
        }

        public void PlayHomeBackgroundSound()
        {
            if(isSoundOn)
            {
                StopOtherBGMusic();
                backgroundMusic.Play();
                bgMusic = backgroundMusic;
            }
        }

        public void PlayGameplaySound()
        {
            if (isSoundOn)
            {
                StopOtherBGMusic();
                gameplayMusic.Play();
                bgMusic = gameplayMusic;
            }
        }

        public void PlayUIClick()
        {
            if (isSoundOn)
            {
                uiClick.Play();
            }
        }

        public void PlayExplosion()
        {
            if (isSoundOn)
            {
                collisionExplosionMusic.Play();
            }
        }

        public void PlayCollect()
        {
            if (isSoundOn)
            {
                shootingSound.Play();
            }
        }

        public void PlayGameover()
        {
            if (isSoundOn)
            {
                gameOverSound.Play();
            }
        }

        public void StopOtherBGMusic()
        {
            if (bgMusic != null)
            {
                bgMusic.Stop();
                bgMusic = null;
            }
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        private void SetSound()
        {
            int soundStatus = PlayerPrefs.GetInt("SoundStatus", 1);
            if(soundStatus == 1)
            {
                isSoundOn = true;
            }else{
                isSoundOn = false;
            }
        }

        private void EnableSound()
        {
            isSoundOn = true;
            PlayHomeBackgroundSound();
            PlayerPrefs.SetInt("SoundStatus", 1);
        }

        private void DisableSound()
        {
            isSoundOn = false;
            StopOtherBGMusic();
            PlayerPrefs.SetInt("SoundStatus", 0);
        }
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
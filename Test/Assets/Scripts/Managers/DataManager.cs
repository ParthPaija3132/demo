﻿using UnityEngine;
using System.Collections;
using System;

namespace tag
{
    public class DataManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static DataManager Instance;
        public UpgardeDataSO shootingSpeedUpgrade;
        public UpgardeDataSO shootingPowerUpgrade;
        public PlayerData playerData;

        //public int highScore { get { return PlayerPrefs.GetInt("highScore", 0); } set { PlayerPrefs.SetInt("highScore", value); } }

        public int score;

        #endregion

        #region PRIVATE_VARS

        private const string PLAYER_DATA = "PlayerData";

        public string PlayerDataPrefs
        {
            get
            {
                return PlayerPrefs.GetString(PLAYER_DATA, string.Empty);
            }
            set
            {
                PlayerPrefs.SetString(PLAYER_DATA, value);
            }
        }

        #endregion

        #region UNITY_CALLBACKS

        void Awake()
        {
            Instance = this;
            Init();
        }

        void OnEnable()
        {
            EventManager.onGameStart += OnGameStart;
            EventManager.onGameOver += OnGameOver;
        }

        void OnDisable()
        {
            EventManager.onGameStart -= OnGameStart;
            EventManager.onGameOver -= OnGameOver;
        }

        private void Start()
        {
            //Init();
        }

        #endregion

        #region PUBLIC_FUNCTIONS

        public void Init()
        {
            LoadPlayerData();
        }

        public void IncreaseScore(int value)
        {
            score += value;
            EventManager.RaiseOnScoreChange(score);
        }

        public void SavePlayerData()
        {
            PlayerDataPrefs = JsonUtility.ToJson(playerData);
        }

        public void OnGameStart()
        {
            score = 0;
        }

        public void OnGameOver()
        {
            if (score > playerData.highScore)
            {
                playerData.highScore = score;
            }
            playerData.totalPoint += score;
            SavePlayerData();
        }

        #endregion

        #region PRIVATE_FUNCTIONS

        private void LoadPlayerData()
        {
            if (string.IsNullOrEmpty(PlayerDataPrefs))
            {
                playerData.SetDefaultData();
                Debug.Log("Set Default Data");
            }
            else
            {
                playerData = JsonUtility.FromJson<PlayerData>(PlayerDataPrefs);
                Debug.Log("Load Previous Data");
            }
            SavePlayerData();
        }

        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }

    [System.Serializable]
    public class PlayerData
    {
        public int highScore;
        public int shootingSpeedUpgradeIndex;
        public int shootingPowerUpgardeIndex;
        public int totalPoint;

        public void SetDefaultData()
        {
            highScore = 0;
            shootingPowerUpgardeIndex = 0;
            shootingSpeedUpgradeIndex = 0;
            totalPoint = 0;
        }
    }
}
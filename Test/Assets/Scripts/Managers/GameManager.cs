﻿using UnityEngine;

namespace tag
{
    public class GameManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static GameManager Instance;
        public GameState gameState = GameState.NONE;

        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS

        void Awake()
        {
            Instance = this;
        }

        #endregion

        #region PUBLIC_FUNCTIONS

        public void StartGame()
        {
            gameState = GameState.GAMEPLAY;
            UIManager.Instance.homeView.HideView();
            UIManager.Instance.upgradeView.HideView();
            UIManager.Instance.gameplayView.ShowView();
            EventManager.RaiseOnGameStart();
        }

        public void GameOver()
        {
            gameState = GameState.NONE;
            UIManager.Instance.gameplayView.HideView();
            EventManager.RaiseOnGameOver();
            UIManager.Instance.upgradeView.ShowView();
            UIManager.Instance.gameOverView.ShowView();
        }

        public void PauseGame()
        {
            gameState = GameState.GAMEPAUSE;
            Time.timeScale = 0;
            UIManager.Instance.pauseView.ShowView();
        }

        public void ResumeGame()
        {
            gameState = GameState.GAMEPLAY;
            Time.timeScale = 1;
            UIManager.Instance.pauseView.HideView();
        }

        public void RestartGame()
        {
            gameState = GameState.GAMEPLAY;
            UIManager.Instance.homeView.HideView();
            UIManager.Instance.gameplayView.ShowView();
            UIManager.Instance.upgradeView.HideView();
            EventManager.RaiseOnGameStart();
        }

        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }

    public enum GameState
    {
        NONE,
        GAMEPLAY,
        GAMEPAUSE
    }
}
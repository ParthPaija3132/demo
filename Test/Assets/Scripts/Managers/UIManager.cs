﻿using UnityEngine;
using System.Collections;

namespace tag
{
    public class UIManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static UIManager Instance;
        public HomeView homeView;
        public GameplayView gameplayView;
		public PauseView pauseView;
		public GameOverView gameOverView;
        public FlashView flashView;
        public UpgradeView upgradeView;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        void Awake()
        {
            Instance = this;
//            homeView.ShowView();
        }

        //private void OnEnable()
        //{
        //    homeView.ShowView();
        //}

        private void Start()
        {
            homeView.ShowView();
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
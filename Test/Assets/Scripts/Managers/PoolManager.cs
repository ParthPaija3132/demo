﻿using UnityEngine;
using System.Collections;
using Pool = global::PoolManager;

namespace tag
{
    public class PoolManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static PoolManager Instance;
//        public Transform prefabTestObj;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        void Awake()
        {
            Instance = this;
        }
        #endregion

        #region PUBLIC_FUNCTIONS
		public Transform SpawnObstacle(Transform t)
		{
			return Pool.Pools["Obstacle"].Spawn(t);
		}
		public void DespawnObstacle(Transform t)
		{
			Pool.Pools["Obstacle"].Despawn(t);
		}
		public void DespawnAllObstacle()
		{
			Pool.Pools["Obstacle"].DespawnAll();
		}

        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
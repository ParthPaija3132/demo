﻿using UnityEngine;
using System.Collections;

namespace tag
{
    public class LevelManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static LevelManager Instance;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        void Awake()
        {
            Instance = this;
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}
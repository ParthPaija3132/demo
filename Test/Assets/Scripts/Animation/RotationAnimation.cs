﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tag
{
	public class RotationAnimation : MonoBehaviour {

        #region PUBLIC_VARS
        public RectTransform transformToAnimate;
        public Vector2 animationTime;
        public AnimationCurve animationCurve;
        #endregion

        #region PRIVATE_VARS
        private Coroutine animationCoroutine;
		#endregion

		#region UNITY_CALLBACKS
		private void Awake()
		{
            if(transformToAnimate == null)
            {
                transformToAnimate = this.gameObject.GetComponent<RectTransform>();
            }
		}

		private void OnEnable()
		{
            if(animationCoroutine == null)
            {
                animationCoroutine = StartCoroutine(RotationCoroutine());
            }else{
                StopCoroutine(animationCoroutine);
                animationCoroutine = null;
                animationCoroutine = StartCoroutine(RotationCoroutine());
            }
		}

		private void OnDisable()
		{
            if (animationCoroutine != null)
            {
                StopCoroutine(animationCoroutine);
                animationCoroutine = null;
            }
		}
		#endregion

		#region PUBLIC_FUNCTIONS

		#endregion

		#region PRIVATE_FUNCTIONS

		#endregion

		#region CO-ROUTINES
        IEnumerator RotationCoroutine()
        {
            float t = Random.Range(0f,1f);
            float rate = 1 / Random.Range(animationTime.x,animationTime.y);

            while(true)
            {
                while(t <= 1f)
                {
                    t += rate * Time.unscaledDeltaTime;
                    transformToAnimate.localEulerAngles = new Vector3(0f,0f,Mathf.Lerp(0,360,animationCurve.Evaluate(t))); 
                    yield return 0;
                }
                t = 0;
            }
        }
		#endregion

		#region EVENT_HANDLERS

		#endregion

		#region UI_CALLBACKS

		#endregion
	}
}
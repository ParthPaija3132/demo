﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace tag
{
	public class ColorLerpText : MonoBehaviour {

        #region PUBLIC_VARS
        public Text textToLerp;
        public float animationTime;
        public List<Color> colors;
        #endregion

        #region PRIVATE_VARS
        private Coroutine colorLerpCoroutine;
		#endregion

		#region UNITY_CALLBACKS
		private void OnEnable()
		{
            StartCoroutineLerp();
		}
		#endregion

		#region PUBLIC_FUNCTIONS
        public void StartCoroutineLerp()
        {
            if(colorLerpCoroutine == null)
            {
                colorLerpCoroutine = StartCoroutine(ColorLerpCoroutine());
            }else{
                StopCoroutine(colorLerpCoroutine);
                colorLerpCoroutine = StartCoroutine(ColorLerpCoroutine());
            }
        }
		#endregion

		#region PRIVATE_FUNCTIONS

		#endregion

		#region CO-ROUTINES
        IEnumerator ColorLerpCoroutine()
        {
            float t = 0;
            float rate = 1 / animationTime;
            int index = 0;
            textToLerp.color = colors[index];
            Color currentColor = textToLerp.color;

            while(true)
            {
                currentColor = textToLerp.color;
                while(t <= 1)
                {
                    t += rate * Time.deltaTime;
                    textToLerp.color = Color.Lerp(currentColor, colors[index], t);
                    yield return 0;
                }

                t = 0;
                index++;
                if(index == colors.Count)
                {
                    index = 0;
                }
            }
        }
		#endregion

		#region EVENT_HANDLERS

		#endregion

		#region UI_CALLBACKS

		#endregion
	}
}
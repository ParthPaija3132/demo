﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tag
{
    public class ScaleAnimation : MonoBehaviour
    {
        #region PUBLIC_VARS
        public RectTransform transformToAnimate;
        public float animationTime;
        public AnimationCurve animationCurve;
        public Vector2 scaleRange;
        #endregion

        #region PRIVATE_VARS
        private Coroutine animationCoroutine;
        #endregion

        #region UNITY_CALLBACKS
        private void OnEnable()
        {
            if (animationCoroutine == null)
            {
                animationCoroutine = StartCoroutine(ScaleCoroutine());
            }
            else
            {
                StopCoroutine(animationCoroutine);
                animationCoroutine = null;
                animationCoroutine = StartCoroutine(ScaleCoroutine());
            }
        }

        private void OnDisable()
        {
            if (animationCoroutine != null)
            {
                StopCoroutine(animationCoroutine);
                animationCoroutine = null;
            }
        }
        #endregion

        #region PUBLIC_FUNCTIONS

        #endregion

        #region PRIVATE_FUNCTIONS

        #endregion

        #region CO-ROUTINES
        IEnumerator ScaleCoroutine()
        {
            float t = 0;
            float rate = 1 / animationTime;

            while (true)
            {
                while (t <= 1f)
                {
                    t += rate * Time.deltaTime;
                    transformToAnimate.localScale = Vector3.one * Mathf.Lerp(scaleRange.x,scaleRange.y,animationCurve.Evaluate(t));
                    yield return 0;
                }
                t = 0;
            }
        }
        #endregion

        #region EVENT_HANDLERS

        #endregion

        #region UI_CALLBACKS

        #endregion
    }
}
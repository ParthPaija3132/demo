﻿using UnityEngine;
using System.Collections;
using System;

namespace tag
{
    [RequireComponent(typeof(Camera))]
    public class CameraWidthSetter : MonoBehaviour
    {
        #region PUBLIC_VARS
        public Camera mainCamera;
        public float worldWidth = 29.25f;
        public bool isWidthSet;
        #endregion

        #region PRIVATE_VARS

        #endregion

        #region UNITY_CALLBACKS

        void Awake()
        {
            if(mainCamera == null)
            {
                mainCamera = this.gameObject.GetComponent<Camera>();
            }
            //CameraSetup();
            MakeSetup();
        }

        //public void MakeSetup(float width)
        //{
        //    CameraSetup(width);
        //}

        public void MakeSetup()
        {
            if(isWidthSet)
            {
                CameraSetupWidth(worldWidth);
            }else{
                CameraSetupHeight(worldWidth);
            }
        }

        #endregion

        #region PUBLIC_FUNCTIONS

        #endregion

        #region PRIVATE_FUNCTIONS

        private void CameraSetup()
        {
            float x = worldWidth;
            float y;
            if (mainCamera.aspect < 1f)
                y = x * Screen.height / Screen.width;
            else
                y = x * Screen.width / Screen.height;
            mainCamera.orthographicSize = y * 0.5f;
        }

        private void CameraSetupWidth(float width)
        {
            float x = width;
            float y;
            if (mainCamera.aspect < 1f)
            {
                y = x * Screen.height / Screen.width;
            }
            else
            {
                y = x * Screen.width / Screen.height;
            }
            mainCamera.orthographicSize = y * 0.5f;
        }

        private void CameraSetupHeight(float hieght)
        {
            float x;
            float y = hieght;
            if (mainCamera.aspect < 1f)
            {
                Debug.Log("Less");
                x = y * Screen.width / Screen.height;
            }
            else
            {
                Debug.Log("More");
                x = y * Screen.height / Screen.width;
            }
            mainCamera.orthographicSize = x * 0.5f;
        }

        #endregion

        #region CO-ROUTINES

        #endregion

        #region EVENT_HANDLERS

        #endregion

        #region UI_CALLBACKS

        #endregion
    }
}